#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
struct node {
  double x;
  /*struct node *prev;*/
  struct node *left;
  struct node *right;
};

int max(int a,int b){
	if(a>b) return a;
	return b;
}

void freem(struct node*root){
	if(root==NULL) return;
	freem(root->left);
	freem(root->right);
	free(root);
}

int main(){
    while(1){
      int heightt=0;
while(getchar()!='\n'){}
{int j=0;
  clock_t tStart=clock();

    unsigned int N=1000000,i=0,flag,h=0,height=0;
    while(((double)(clock() - tStart)/CLOCKS_PER_SEC)<1.0){j++;
    /* This won't change, or we would lose the list in memory */
    struct node *root;
    /* This will point to each node as it traverses the list */
    struct node *conductor;

    root = malloc( sizeof(struct node) );
    /*root->prev = 0;*/
    root->left=NULL;
    root->right=NULL;
    i=0;
    h=0;
    height=0;
    double val;
    //scanf("%d",&N);
    time_t t;
   /* Intializes random number generator */
   srand((unsigned) time(&t));
   //clock_t tStart = clock();

    	root->x = (double)rand()/(RAND_MAX+1);

    for(i=1;i<N;i++){
        conductor = root;
        flag = 0;
        val = (double)rand()/(RAND_MAX+1);
        h=1;
          while(flag==0){
          	h++;
          if(conductor->x < val){

               if(conductor->right == NULL){
                   conductor->right = malloc(sizeof(struct node));
		   conductor = conductor->right;
                   conductor->left = NULL;
                   conductor->right = NULL;
                   conductor->x = val;
                   flag = 1;
               }
	       else conductor = conductor->right;
           }
            else{

                if(conductor->left == NULL){
                   conductor->left = malloc( sizeof(struct node) );
		   conductor = conductor-> left;
                   conductor->left = NULL;
                   conductor->right = NULL;
                   conductor->x = val;
                   flag = 1;
               }
	       else conductor = conductor-> left;
            }
        }
   	height = max(h,height);
    }
    heightt = heightt + height;
    //freem(root);
    free(root);
}
printf("count = %d\n", j);
printf("height average = %lf\n",(double)heightt/j );
double temp= (double)(clock() - tStart)/CLOCKS_PER_SEC;
    printf("T(%d) = %lf\n",N, temp);
  }}
}