#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <strings.h>
#include <time.h>
#include <cstdlib>
#include <algorithm>
	
using namespace std;

struct xint{
	bool active;
	int size;
	double x1;
	double x2;
	double y;
};

struct yint{
	bool active;
	int size;
	double y1;
	double y2;
	double x;
};

struct interval{
	double val;
	int int_num;
};

struct node {
	double val;
	int int_num;
	int size;
	bool active;
	struct node *parent;
	struct node *left;
	struct node *right;
};

bool compare_int(interval A, interval B){
	if(A.val < B.val) return 1;
	else if(A.val > B.val) return 0;
	else if(A.int_num < B.int_num) return 1;
	else return 0;
}

bool compare_xint(xint A, xint B){
	return A.y < B.y;
}

bool compare_yint(yint A, yint B){
	return A.x < B.x;
}

void generateX(xint* A, int n){
	int count = n;
	while(count--){
		double a = (double)rand()/(double)RAND_MAX;
		double b = (double)rand()/(double)RAND_MAX;
		if(a>b) swap(a,b);
		A[count].x1 = a;
		A[count].x2 = b;
		A[count].y = (double)rand()/(double)RAND_MAX;
		//printf("x1 = %lf x2 = %lf y = %lf\n", A[count].x1,A[count].x2, A[count].y);
		/*double a, b;
		scanf("%lf %lf %lf", &a, &b, &A[count].y);
		if(a>b) swap(a,b);
		A[count].x1 = a;
		A[count].x2 = b;*/
		//printf("X: %lf %lf %lf\n", A[count].x1, A[count].x2, A[count].y);
		
	}
	return;
}

void generateY(yint* A, int n){
	int count = n;
	while(count--){
		double a = (double)rand()/(double)RAND_MAX;
		double b = (double)rand()/(double)RAND_MAX;
		if(a>b) swap(a,b);
		A[count].y1 = a;
		A[count].y2 = b;
		A[count].x = (double)rand()/(double)RAND_MAX;
		//printf("y1 = %lf y2 = %lf x = %lf\n", A[count].y1,A[count].y2, A[count].x);
		/*	double a, b;
		scanf("%lf %lf %lf", &a, &b, &A[count].x);
		if(a>b) swap(a,b);
		A[count].y1 = a;
		A[count].y2 = b;*/
		//printf("Y: %lf %lf %lf\n", A[count].y1, A[count].y2, A[count].x);
	}
	return;
}

node* maketree(yint* Y, int n, int offset, node* parent){
	if(n<=0) return NULL;
	node* tree = (node*)malloc(sizeof(node));
	tree->val = Y[offset + (n-1)/2].x;
	tree->int_num = offset + (n-1)/2;
	tree->parent = parent;
	tree->size = 0;
	tree->active = false;
	tree->left = maketree(Y, (n-1)/2, offset, tree);
	tree->right = maketree(Y, n/2, offset + (n+1)/2, tree);
	return tree;
}

void traverse(node* tree){
	if(tree==NULL) return;
	//printf("Val: %lf | Int_Num = %d active = %d size = %d\n", tree->val, tree->int_num, tree->active, tree->size);
	traverse(tree->left);
	traverse(tree->right);
}

node* search(node* tree, double val, int int_num){
	node* current = tree;
	while(true){
		//printf("%d\n",current->int_num);
		if(current==NULL) {
			break;
		}
		if(current->val < val){
			current = current->right;
		}
		else if(current->val > val){
			current = current->left;
		}
		else if(current->int_num == int_num){
			break;
		}
		else if(current->int_num < int_num){
			current = current->right;
		}
		else{
			current = current->left;
		}
	}
	node* ans = current;
	if(current->active==false){
		current->active = true;
		while(current!=NULL){
			current->size++;
			current = current->parent;
		}
	}
	else{
		current->active = false;
		while(current!=NULL){
			current->size--;
			current = current->parent;
		}
	}
	return ans;
}

int intCount(node* tree, double x1, double x2){
	//printf("intCount %lf %lf\n",x1,x2);
	node* LCA = tree;
	int size=0;
	while(true){
		if(LCA==NULL) return 0;
		//printf("LCA: %d\n", LCA->int_num);
		if(LCA->val < x1 && LCA->val < x2){
			LCA = LCA->right;
		}
		else if(LCA->val > x1 && LCA->val > x2){
			LCA = LCA->left;
		}
		else break;
	}
	size = size + (int)LCA->active;
	node* left = LCA->left;
	node* right = LCA->right;
	while(true){
		//printf("%d\n",current->int_num);
		if(left==NULL) break;
		//printf("num: %d size: %d, ans: %d\n",left->int_num, left->size, size);
		if(left->val < x1){
			left = left->right;
		}
		else{
			if(left->right!=NULL){
				size = size + (left->right)->size + (int)left->active;
			}
			else if(left->left==NULL){
				size = size + left->size;
			}
			else size = size + (int)left->active;
			left = left->left;
		}
	}
	while(true){
		//printf("%d\n",current->int_num);
		if(right==NULL) break;
		//printf("num: %d size: %d ans: %d\n",right->int_num, right->size, size);
		if(right->val > x2){
			right = right->left;
		}
		else{
			if(right->left!=NULL){
				size = size + (right->left)->size + (int)right->active;
			}
			else if(right->right==NULL){
				size = size + right->size;
			}
			else size = size + (int)right->active;
			right = right->right;
		}
		/*else if(right->int_num == int_num){
			if(right->left=NULL){
				size = size + (right->left)->size;
			}
			break;
		}
		else if(right->int_num < int_num){
			right = right->right;
			if(right->left=NULL){
				size = size + (right->left)->size;
			}
		}
		else{
			right = right->left;
		}*/
	}
	return size;
}

long long int bruteforce(xint* X, yint* Y, int n){
	int i, j, count = 0;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			if(X[i].x1 <= Y[j].x && X[i].x2 >= Y[j].x && Y[j].y1 <= X[i].y && Y[j].y2 >= X[i].y){
				//printf("Adding: %lf %lf %lf & %lf %lf %lf\n", X[i].x1, X[i].x2, X[i].y, Y[i].y1, Y[i].y1, Y[i].x);
				count++;
			}
			else{
				//printf("Not :%lf %lf %lf & %lf %lf %lf\n", X[i].x1, X[i].x2, X[i].y, Y[i].y1, Y[i].y1, Y[i].x);
			}
		}
	}
	return count;
}

void acivate_upper(node* tree, yint* Y, interval* A, int n, double y){
	int i;
	for (i = 0; i < n; i++){
		if(Y[A[i].int_num].y1==y){
			int int_num = A[i].int_num;
			double val = Y[int_num].x;
			node* current = tree;
			while(true){
				if(current==NULL) {
					//printf("Hii val: %lf int_num: %d interval %lf %lf\n",val, int_num, Y[int_num].y1,Y[int_num].y2);
					break;
				}
				else if(current->val < val){
					current = current->right;
				}
				else if(current->val > val){
					current = current->left;
				}
				else if(current->int_num == int_num){
					break;
				}
				else if(current->int_num < int_num){
					current = current->right;
				}
				else{
					current = current->left;
				}
				//printf("%d %d %lf %lf\n",int_num, current->int_num, current->val, val);
			}
			//printf("current: %lf,%d",current->val,current->int_num);
			if(current->active==false){
				current->active = true;
				while(current!=NULL){
					current->size++;
					current = current->parent;
				}
			}
		}
	}
}

void deactivate_lower(node* tree, yint* Y, interval* A, int n, double y){
	int i;
	for (i = 0; i < n; i++){
		if(Y[A[i].int_num].y2==y){
			int int_num = A[i].int_num;
			double val = Y[int_num].x;
			node* current = tree;
			while(true){
				//printf("%d\n",current->int_num);
				if(current==NULL) {
					return;
					break;
				}
				if(current->val < val){
					current = current->right;
				}
				else if(current->val > val){
					current = current->left;
				}
				else if(current->int_num == int_num){
					break;
				}
				else if(current->int_num < int_num){
					current = current->right;
				}
				else{
					current = current->left;
				}
			}
			if(current->active==true){
				current->active = false;
				while(current!=NULL){
					current->size--;
					current = current->parent;
				}
			}
		}
	}
}

int count_point(node* tree, xint* X, interval*A, int n, double val){
	int i, count = 0;
	for (i = 0; i < n; i++)
	{
		count = count + intCount(tree,X[A[i].int_num].x1,X[A[i].int_num].x2);
	}
	return count;
}
int main(){
	FILE *intersections, *ti, *nSqaure, *ratio, *bruteforces;
	//intersections = fopen("intersections2.txt", "w");
	//ti = fopen("time2.txt", "w");
	nSqaure = fopen("nSuare2.txt", "w");
	//ratio = fopen("ratio2.txt", "w");
	//bruteforces = fopen("bruteforce.txt", "w");
	int n;
	n = 5;
	while(n<100001){
		int j;
		double brutetime;
		clock_t start, end, bstart, bend;
		long long int count = 0;
		xint* X = (xint*)malloc(sizeof(xint)*n);
		yint* Y = (yint*)malloc(sizeof(yint)*n);
		interval* horizontal = (interval*)malloc(sizeof(interval)*n);
		interval* vertical = (interval*)malloc(sizeof(interval)*2*n);
		start=clock();
		for(j=0;j<10;j++){
			//int n;
			putchar('\n');
			//scanf("%d",&n);
			srand(time(NULL));
			generateX(X, n);
			generateY(Y, n);
			sort(Y, Y+n, compare_yint);
			sort(X, X+n, compare_xint);
			int i;
			for(i=0;i<n;i++){
				horizontal[i].val = X[i].y;
				horizontal[i].int_num = i;
				vertical[i].val = Y[i].y1;
				vertical[i].int_num = i;
				vertical[n+i].val = Y[i].y2;
				vertical[n+i].int_num = i;
			}
			sort(horizontal,horizontal+n,compare_int);
			sort(vertical,vertical+2*n,compare_int);
			/*for(i=0;i<n;i++){
				printf("%lf %d %lf %lf\n",Y[i].x, Y[i].y1, Y[i].y2,i);
			}*/
			node* tree = maketree(Y,n,0,NULL);
			//traverse(tree);
			/*for(i=0;i<n;i++){
				node* A = search(tree,Y[i].x,i);
				printf("%lf, %d\n",A->val,A->int_num);
				printf("Count %d\n",intCount(tree,0,1));
			}*/
			int hindex=0;
			int vindex=0;
			//long long int count = 0;
			double vval;
			double hval;
			interval h;
			interval v;
			//traverse(tree);
			while(hindex!=n || vindex!=2*n){
				if(hindex!=n && vindex!=2*n){
					h = horizontal[hindex];
					v = vertical[vindex];
					hval = h.val;
					vval = v.val;
					if(vval<hval){
						vindex++;
						search(tree,Y[v.int_num].x,v.int_num);
						//printf("v no over %lf \n",v.val);
					}
					else if(vval>hval){
						hindex++;
						count = count + intCount(tree,X[h.int_num].x1,X[h.int_num].x2);
						//printf("h no over %lf \n",h.val);
					}
					else{
						//printf("<<<SPECIAL>>> %lf\n",vval);
						int initialh = hindex;
						int initialv = vindex;
						while(horizontal[hindex].val==horizontal[initialh].val) hindex++;
						while(vertical[vindex].val==vertical[initialv].val) vindex++;
						//printf("%lf, %lf\n",horizontal[initialh].val,vertical[initialv].val);
						//traverse(tree);
						acivate_upper(tree, Y, &vertical[initialv], vindex-initialv, vval);
						//traverse(tree);
						count = count + count_point(tree, X, &horizontal[initialh], hindex-initialh, vval);
						deactivate_lower(tree, Y, &vertical[initialv], vindex-initialv, vval);
						//traverse(tree);
					}
				}
				else if(hindex==n){
					v = vertical[vindex];
					vindex++;
					search(tree,Y[v.int_num].x,v.int_num);
					//printf("h    over %lf \n",v.val);
				}
				else{
					h = horizontal[hindex];
					hindex++;
					count = count + intCount(tree,X[h.int_num].x1,X[h.int_num].x2);
					//printf("v    over %lf \n",h.val);
				}
				//traverse(tree);
				//printf("count = %d\n",count);
			}
			if(j==0){
				bstart = clock();
				//printf("%lld",bruteforce(X,Y,n));
				bend = clock();
				brutetime = ((double) bend-bstart)/CLOCKS_PER_SEC;
			}
			
		}
		end = clock();
		double timetaken = ((double) end-start)/CLOCKS_PER_SEC;
		timetaken = (timetaken-brutetime)/10;
		count = count/10;
		printf("%lld %lf\n",count, timetaken);
		//fprintf(intersections, "%d %lld\n", n, count);
		//fprintf(bruteforces, "%lf %lf\n", brutetime, timetaken);
		//fprintf(ti, "%d %lf\n", n, timetaken);
		double ratioNSquare = ((n*1.0)/count)*n;
		double ratioNLogn = ((timetaken*1000000)/log2(n))/n;
		//fprintf(ratio, "%d %lf\n", n, ratioNLogn);
		fprintf(nSqaure, "%d %lf\n", n, ratioNSquare);
		//traverse(tree);
		//break;
		n = max((int)(1.1*n),n+1);
	}
	return 0;
}
