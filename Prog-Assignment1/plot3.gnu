#!/usr/bin/gnuplot

#set term postscript eps enhanced 12
set term postscript eps enhanced color 24

set output 'timenlognratioVsn.eps'
set xlabel "n"
set ylabel "(1000000 * time)/n logn"
set xtics 0,10000
set xtics font "Times-Roman, 10"
set ytics font "Times-Roman, 10" 


set title "time/nlogn ratio Vs n"

set key top right

plot 'ratio2.txt' u 1:2 title "" w lp
#, 'gau.txt' u 1:2 title "Gaussian" w lp, 'poi.txt' u 1:2 title "Poisson" w lp
#plot 'data' u 1:($1*2) title "Double" w lp, 'data' u 1:($1*$1) title "Quadratic" w lp
#plot 'data' u 0:2 title "Legend" w lp	# 0 is the index column
