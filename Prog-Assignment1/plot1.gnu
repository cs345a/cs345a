#!/usr/bin/gnuplot

#set term postscript eps enhanced 12
set term postscript eps enhanced color 24

set output 'intersectionsVSn.eps'
set xlabel "n"
set ylabel "intersections"
set xtics 0,10000
set xtics font "Times-Roman, 10"
set ytics font "Times-Roman, 10" 


#set title "intersections versus n"

set key top right

plot 'intersections2.txt' u 1:2 title "" w lp
#, 'gau.txt' u 1:2 title "Gaussian" w lp, 'poi.txt' u 1:2 title "Poisson" w lp
#plot 'data' u 1:($1*2) title "Double" w lp, 'data' u 1:($1*$1) title "Quadratic" w lp
#plot 'data' u 0:2 title "Legend" w lp	# 0 is the index column
